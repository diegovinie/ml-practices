data = load('ex1data1.txt');
x = data(:, 1);
y = data(:, 2);
m = length(y);
X = [ones(m, 1), data(:, 1)];

thetas = [
  0 0
  -1 2
  0 1
  1 0
  1 1
  1 2
  1 0.5
]';

a1 = computeCost(X, y, thetas(:, 1));
a2 = computeCost(X, y, thetas(:, 2));
a3 = computeCost(X, y, thetas(:, 3));
a4 = computeCost(X, y, thetas(:, 4));

% -----------------------------------------------

iterations = 1500;
alpha = 0.01;

[th, his] = gradientDescent(X, y, [0; 0], alpha, iterations);

th