
file_name = 'test1.csv';

data = csvread('test1.csv');

X = data(:, 2);
y = data(:, 3);
m = length(y);
x_max = max(X);
x_min = min(X);
y_max = max(y);
y_min = min(y);

G = data(:, 2:3);

function Y = filterLT(X, max)
  i = 1;
  j = 1;
  Y = zeros(length(X), 1);
  X = X';

  for x = X,
    if (X(i) < max),
      Y(j) = X(i);
      j = j + 1;
    endif
    i = i + 1;
  end

  Y = Y(1:j-1);
end

function Y = prevF(X, max, index)
  i = 1;
  j = 1;
  Y = zeros(size(X));
  max
  X(1,index)

  for x = X(:, index),
    % max - x
    if (x < max),
      x
      Y(i,:) = X(i,:);
      j = j + 1;
    endif
    i = i + 1;
  end

  Y = Y(1:j-1, :);
end

function Y = filterGT(X, min)
  i = 1;
  j = 1;
  Y = zeros(length(X), 1);
  X = X';

  for x = X,
    if (X(i) > min),
      Y(j) = X(i);
      j = j + 1;
    endif
    i = i + 1;
  end

  Y = Y(1:j-1);
end



yf = filterLT(y, 900e6);

yf = filterGT(y, 100e6);

% plot(X, y, 'rx');
% hold on;
axis([0, x_max, 0, y_max], 'square');

GG = prevF(G, 700e6, 2);


plot(GG(:,2));
