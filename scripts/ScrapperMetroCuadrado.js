
function getPubByPage (responseBody) {
  const $ = cheerio.load(responseBody);
  const items = $('#main .m_rs_list_item');

  const datas = items.map(function () {
      const $el = cheerio(this);

      const priceEl = $el.find('span[itemprop="price"]');
      const headEl = $el.find('.detail_wrap');
      const descEl = $el.find('.desc p[itemprop="description"]');
      const sizeEl = $el.find('.desc_rs .m2 span:not(.item_title)');
      const roomsEl = $el.find('.desc_rs .rooms span:not(.item_title):not(.item_title_mobile)');
      const bathroomsEl = $el.find('.desc_rs .bathrooms:not(.garages) span:not(.item_title):not(.item_title_mobile)');
      const garagesEl = $el.find('.desc_rs .garages span');


      return {
        id: headEl.attr('id'),
        neighborhood: headEl.attr('neighborhood'),
        price: priceEl.text(),
        size: sizeEl.text(),
        rooms: roomsEl.text(),
        bathrooms: bathroomsEl.text(),
        garages: garagesEl.text(),
        description: descEl.text()
      };
  });

  const res = datas.get();

  // console.log(res);
  console.log(JSON.stringify(res));

  return res;
}
