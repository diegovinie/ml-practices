const fs = require('fs');
const SAMPLES_DIR = __dirname + '/../fixtures/apts/m2/cedritos-contador2';
const OUTPUT_FILE_PATH = __dirname + '/../fixtures/apts/m2/test1.csv';
const ATTRIBUTES = ['id', 'size'];

prepareData();

// ---------------------------------------

function prepareData () {
  getFilePaths()
    .then(files => Promise.all(files.map(operateOnSampleFile))
      .then(csvStrings => csvStrings.join(''))
    )
    .then(saveCsvFile);
}

function operateOnSampleFile (path) {
  return getFileContent(path)
    .then(str => JSON.parse(str))
    .then(parseObjectsToCsv)
}

function getFilePaths () {
  console.log('Reading from: ', SAMPLES_DIR, '\n');

  return new Promise((resolve, reject) => {
    fs.readdir(SAMPLES_DIR, (err, files) => {
      if (err) return reject(err);

      const filteredFiles = files.filter(file => /\.json$/.test(file))

      console.log('Selected files: ', filteredFiles, '\n');

      resolve(filteredFiles);
    });
  });
}

function getFileContent (filename) {
  return new Promise((resolve, reject) => {
    fs.readFile(SAMPLES_DIR + '/' + filename, (err, data) => {
      if (err) return reject(err);

      resolve(data);
    });
  });
}

function filterAttributes (items) {
  return items.map(item => Object.entries(item)
    .filter(([k, v]) => ATTRIBUTES.includes(k))
    .reduce((acc, [k, v]) => ({...acc, [k]: v}), {})
  );
}

function parseObjectsToCsv (itemList) {
  const items = itemList.map(item => ({
    id: item.id,
    size: item.size ? item.size.replace('m2', '').trim() : null,
    price: item.price ? item.price.replace(/[\.\$]/g, '').trim() : null
  }));

  return items
    .map(item => `${item.id}, ${item.size}, ${item.price}`)
    .join('\n')
    .concat('\n');
}

function saveCsvFile (content) {
  console.log('Saving as: ', OUTPUT_FILE_PATH, '\n');

  return new Promise((resolve, reject) => {
    fs.writeFile(OUTPUT_FILE_PATH, content, 'utf8', err => {
      if (err) throw err;
    });
  });
}
